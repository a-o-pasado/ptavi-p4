#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import socket

if len(sys.argv) != 6:
    print("Usage: client.py ip puerto register sip_address expires_value")
    sys.exit()

SERVER = 'localhost'
IP = str(sys.argv[1])
PORT = int(sys.argv[2])
METODO = sys.argv[3].upper()
ADDRESS = sys.argv[4]
EXPIRES = sys.argv[5]
REQUEST = METODO + " sip:" + ADDRESS + " " + "SIP/2.0" "\r\n" + EXPIRES + "\r\n"
my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
my_socket.connect((SERVER, PORT))

print("Se esta Mandando: " + REQUEST)
my_socket.send(bytes(REQUEST, 'utf-8') + b'\r\n')
data = my_socket.recv(1024)

print('Recibido -- ', data.decode('utf-8'))
print("Se esta Acabando el socket...")

my_socket.close()
print("Terminado")
