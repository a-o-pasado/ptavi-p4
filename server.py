#!/usr/bin/python
# -*- coding: utf-8 -*-

import socketserver
import sys
import json
from time import gmtime, strftime, time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    dicc = {}

    def register2json(self):

        print("registerToJson")
        nameJson = "registered.json"
        with open(nameJson, 'w') as fichero:
            json.dump(self.dicc, fichero)

    def json2registered(self):

        print("jsonToRegistered")
        nameJson = "registered.json"
        try:
            with open(nameJson, 'r') as fichero:
                self.dicc = json.load(fichero)
        except:
            print("Error on registered.json file")

    def eliminarDicc(self):

        lista = []
        formato = "%a, %d %b %Y %H:%M:%S +0000"

        for sublista in self.dicc:
            expire = " ".join(self.dicc[sublista][1].split(" ")[1:])
            tiempoAct = strftime(formato, gmtime())
            if expire <= tiempoAct:
                lista.append(sublista)

        for usuario in lista:
            del self.dicc[usuario]

    def handle(self):

        ip = self.client_address[0]
        port = self.client_address[1]

        self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
        if len(self.dicc) == 0:
            self.json2registered()
        while 1:

            line = self.rfile.read()
            metodo = line.decode('utf-8').split(" ")[0]

            if metodo == 'REGISTER':
                user = line.decode('utf-8').split(" ")[1]
                expires = line.decode('utf-8').split(" ")[2].split("\r\n")[1]
                valor = int(expires) + int(time())
                formato = "%a, %d %b %Y %H:%M:%S +0000"
                tiempo = strftime(formato, gmtime(valor))
                address = str(ip) + ":" + str(port)
                self.dicc[user] = ["address: " + address, "expires: " + tiempo]
                if int(expires) == 0:
                    print("Se esta Eliminando: " + user + ":" + str(self.dicc[user]))
                    del self.dicc[user]
                self.eliminarDicc()
                self.register2json()
                print(self.dicc)
            if not line:
                break


if __name__ == "__main__":
    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    print("Lanzando el Servidor")
    serv.serve_forever()
